package com.example.socialauth;

import org.brickred.socialauth.Profile;
import org.brickred.socialauth.android.DialogListener;
import org.brickred.socialauth.android.SocialAuthAdapter;
import org.brickred.socialauth.android.SocialAuthAdapter.Provider;
import org.brickred.socialauth.android.SocialAuthError;
import org.brickred.socialauth.android.SocialAuthListener;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity {

	public static ImageView facebook, twitter, googleplus;
	public static Button fbsignout, twittersignout, googleplussignout;
	public static SocialAuthAdapter fbadapter, twitteradapter, gplusadapter;
	public static String name, location;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		facebook = (ImageView) findViewById(R.id.fbimageview);
	//	fbsignout = (Button) findViewById(R.id.fbsignout);
		twitter = (ImageView) findViewById(R.id.googleimg);
	//	twittersignout = (Button) findViewById(R.id.twittersignout);
		googleplus = (ImageView) findViewById(R.id.twitterImage);
	//	googleplussignout = (Button) findViewById(R.id.googlesignout);
		fbadapter = new SocialAuthAdapter(new fbresponselistener());
		twitteradapter = new SocialAuthAdapter(new twitterresponselistener());
		gplusadapter = new SocialAuthAdapter(new gplusresponselistener());
		gplusadapter.addCallBack(Provider.GOOGLEPLUS,
				"https://www.grinfotech.com/about_us.php");

		facebook.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fbadapter.authorize(MainActivity.this, Provider.FACEBOOK);

			}
		});
/*
		fbsignout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fbadapter.signOut(MainActivity.this,
						Provider.FACEBOOK.toString());
				Toast.makeText(MainActivity.this, "Signing out from fb",
						Toast.LENGTH_LONG).show();
			}
		});
*/		twitter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				twitteradapter.authorize(MainActivity.this, Provider.TWITTER);
			}
		});

/*		twittersignout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				twitteradapter.signOut(MainActivity.this,
						Provider.TWITTER.toString());
				Toast.makeText(MainActivity.this, "Signing out from twitter",
						Toast.LENGTH_LONG).show();
			}
		});
*/		googleplus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				gplusadapter.authorize(MainActivity.this, Provider.GOOGLEPLUS);
			}
		});
/*		googleplussignout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				gplusadapter.signOut(MainActivity.this,
						Provider.GOOGLEPLUS.toString());
				Toast.makeText(MainActivity.this,
						"Signing out from googleplus", Toast.LENGTH_LONG)
						.show();
			}
		});
*/
	}

	class fbresponselistener implements DialogListener {
		@Override
		public void onComplete(Bundle arg0) {
			// TODO Auto-generated method stub
			fbadapter.getUserProfileAsync(new SocialAuthListener<Profile>() {

				@Override
				public void onExecute(String arg0, Profile profileMap) {
					// TODO Auto-generated method stub
					Log.d("test",
							"Validate ID         = "
									+ profileMap.getValidatedId());
					Log.d("test",
							"First Name          = "
									+ profileMap.getFirstName());
					Log.d("test",
							"Last Name           = " + profileMap.getLastName());
					Log.d("test",
							"Email               = " + profileMap.getEmail());
					Log.d("test",
							"Gender                   = "
									+ profileMap.getGender());
					Log.d("test",
							"Country                  = "
									+ profileMap.getCountry());
					Log.d("test",
							"Language                 = "
									+ profileMap.getLanguage());
					Log.d("test",
							"Location                 = "
									+ profileMap.getLocation());
					Log.d("test",
							"Profile Image URL  = "
									+ profileMap.getProfileImageURL());
					name = profileMap.getFirstName() + " "
							+ profileMap.getLastName();
					location = profileMap.getCountry();
					Intent i = new Intent(MainActivity.this, NextActivity.class);
					i.putExtra("name", name);
					i.putExtra("location",location);
					i.putExtra("flag",1);
					startActivity(i);

				}

				@Override
				public void onError(SocialAuthError arg0) {
					// TODO Auto-generated method stub
					Log.i("test", "the error is " + arg0);
				}
			});

		}

		@Override
		public void onBack() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onCancel() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onError(SocialAuthError arg0) {
			// TODO Auto-generated method stub
			Log.i("test", "the err 	or is " + arg0);

		}

	}

	class twitterresponselistener implements DialogListener {

		@Override
		public void onBack() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onCancel() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onComplete(Bundle arg0) {
			// TODO Auto-generated method stub
			twitteradapter
					.getUserProfileAsync(new SocialAuthListener<Profile>() {

						@Override
						public void onExecute(String arg0, Profile arg1) {
							// TODO Auto-generated method stub
							Log.d("test", "Receiving Data");
							Profile profileMap = arg1;
							Log.d("test",
									"Validate ID         = "
											+ profileMap.getValidatedId());
							Log.d("test",
									"First Name          = "
											+ profileMap.getFullName());
							Log.d("test", "Language                 = "
									+ profileMap.getLanguage());
							Log.d("test", "Location                 = "
									+ profileMap.getLocation());
							Log.d("test",
									"Profile Image URL  = "
											+ profileMap.getProfileImageURL());
							Log.d("test",
									"Email               = " + profileMap.getEmail());
							Log.d("test",
									"Gender                   = "
											+ profileMap.getGender());
							name = profileMap.getFullName();
							location = profileMap.getLocation();
							Intent i = new Intent(MainActivity.this,
									NextActivity.class);
							i.putExtra("name", name);
							i.putExtra("location", location);
							i.putExtra("flag",3);
							startActivity(i);

						}

						@Override
						public void onError(SocialAuthError arg0) {
							// TODO Auto-generated method stub
							Log.i("test", "the error is " + arg0);

						}
					});
		}

		@Override
		public void onError(SocialAuthError arg0) {
			// TODO Auto-generated method stub
			Log.i("test", "the error is " + arg0);

		}
	}

	class gplusresponselistener implements DialogListener {

		@Override
		public void onBack() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onCancel() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onComplete(Bundle arg0) {
			// TODO Auto-generated method stub
			gplusadapter.getUserProfileAsync(new SocialAuthListener<Profile>() {

				@Override
				public void onExecute(String arg0, Profile profileMap) {
					// TODO Auto-generated method stub
					Log.d("test",
							"Validate ID         = "
									+ profileMap.getValidatedId());
					Log.d("test",
							"First Name          = "
									+ profileMap.getFirstName());
					Log.d("test",
							"Last Name           = " + profileMap.getLastName());
					Log.d("test",
							"Email               = " + profileMap.getEmail());
					Log.d("test",
							"Gender                   = "
									+ profileMap.getGender());
					Log.d("test",
							"Country                  = "
									+ profileMap.getCountry());
					Log.d("test",
							"Location                 = "
									+ profileMap.getLocation());
					Log.d("test",
							"Profile Image URL  = "
									+ profileMap.getProfileImageURL());
					name = profileMap.getFirstName() + " "
							+ profileMap.getLastName();
					location = profileMap.getLocation();
					Intent i = new Intent(MainActivity.this, NextActivity.class);
					i.putExtra("name", name);
					i.putExtra("location", location);
					i.putExtra("flag",2);
					startActivity(i);

				}

				@Override
				public void onError(SocialAuthError arg0) {
					// TODO Auto-generated met hod stub

					Log.d("test", "the error is " + arg0);
				}
			});
		}

		@Override
		public void onError(SocialAuthError arg0) {
			// TODO Auto-generated method stub
			Log.d("test", "the error is " + arg0);
		}

	}

}
