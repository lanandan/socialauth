package com.example.socialauth;

import org.brickred.socialauth.android.SocialAuthAdapter.Provider;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class NextActivity extends Activity {
	TextView name, location, gender;
	Button logout;
	int flag;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.next_activity);
		logout = (Button) findViewById(R.id.button1);
		Bundle extras = getIntent().getExtras();
		String value1 = extras.getString("name");
		String value2 = extras.getString("location");
		flag = extras.getInt("flag");
		Log.i("test", "the location is" + value2);
		name = (TextView) findViewById(R.id.textView3);
		name.setText(value1);
		location = (TextView) findViewById(R.id.textView5);
		location.setText("India");
		gender = (TextView) findViewById(R.id.textView7);
		gender.setText("Male");
		logout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				switch (flag) {
				case 1:
					MainActivity.fbadapter.signOut(NextActivity.this,
							Provider.FACEBOOK.toString());
					Toast.makeText(NextActivity.this, "Signing out from fb",
							Toast.LENGTH_LONG).show();
					Intent i=new Intent(NextActivity.this,MainActivity.class);
					startActivity(i);
					break;
				case 2:
					MainActivity.gplusadapter.signOut(NextActivity.this,
							Provider.GOOGLEPLUS.toString());
					Toast.makeText(NextActivity.this,
							"Signing out from googleplus", Toast.LENGTH_LONG)
							.show();
					Intent i1=new Intent(NextActivity.this,MainActivity.class);
					startActivity(i1);
					break;
				case 3:
					MainActivity.twitteradapter.signOut(NextActivity.this,
							Provider.TWITTER.toString());
					Toast.makeText(NextActivity.this,
							"Signing out from twitter", Toast.LENGTH_LONG)
							.show();
					Intent i2=new Intent(NextActivity.this,MainActivity.class);
					startActivity(i2);
					break;
				}

			}
		});
	}

}
